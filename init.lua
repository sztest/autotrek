-- LUALOCALS < ---------------------------------------------------------
local math, minetest, next, pairs, vector
    = math, minetest, next, pairs, vector
local math_abs, math_floor, math_pi
    = math.abs, math.floor, math.pi
-- LUALOCALS > ---------------------------------------------------------

-- How close to exact (1.0) the player's cursor needs to be on the
-- autotrek HUD center to lock in.
local lockmin = 0.999

-- How long the player needs to be walking continuously before the
-- autotrek HUDs display.
local walktime = 2

-- How long the player needs to be pointing directly at one of the
-- autotrek HUDs to lock into autotrek mode.
local locktime = 1

-- How much angular error is tolerated in lookdir correction.
local lookdirerr = 0.001

-- How much position offset from grid center is tolerated on
-- grid center alignment
local griderr = 0.1

-- How much force to apply to correct player grid offset
local gridforce = 0.5

-- How far away from the player to position the HUD
local huddist = 1000000

-- How much error to tolerate in HUD position before resetting
local huderr = 100

------------------------------------------------------------------------
-- CONSTANTS

-- All allowed trek dirs, starting from East (+X) and
-- rotating in 45 degree increments counterclockwise.
local alldirs = {
	vector.new(1, 0, 0),
	vector.new(1, 0, 1),
	vector.new(0, 0, 1),
	vector.new(-1, 0, 1),
	vector.new(-1, 0, 0),
	vector.new(-1, 0, -1),
	vector.new(0, 0, -1),
	vector.new(1, 0, -1),
}
for k, v in pairs(alldirs) do
	alldirs[k] = vector.normalize(v)
end

-- Some precomputed math constants
local hpi = math_pi / 2
local qpi = math_pi / 4
local root2 = 2 ^ 0.5

------------------------------------------------------------------------
-- HUD MANAGEMENT

local hudset
do
	local hudstate = {}
	minetest.register_on_leaveplayer(function(player)
			hudstate[player:get_player_name()] = nil
		end)
	local hud_elem_type = minetest.features.hud_def_type_field and "type" or "hud_elem_type"
	hudset = function(player, pos, opacity)
		local pname = player:get_player_name()
		local state = hudstate[pname]
		if not state then
			state = {}
			hudstate[pname] = state
		end
		if pos then
			local img = "autotrek_hud.png^[opacity:" .. (opacity or 255)
			if not state.id then
				state.id = player:hud_add({
						[hud_elem_type] = "image_waypoint",
						world_pos = pos,
						text = img,
						scale = {x = 0.5, y = 0.5},
						precision = 0,
						number = 0xffffff,
						z_index = -250,
						alignment = {x = 0, y = 0}
					})
				state.pos = pos
				state.img = img
			else
				if vector.distance(pos, state.pos) > huderr then
					player:hud_change(state.id, "world_pos", pos)
					state.pos = pos
				end
				if state.img ~= img then
					player:hud_change(state.id, "text", img)
					state.img = img
				end
			end
		elseif state.id then
			player:hud_remove(state.id)
			state.id = nil
		end
	end
end

------------------------------------------------------------------------
-- PARTICLE EFFECTS

local oldparticles = {}

local function particles(player, pname, speed)
	pname = pname or player:get_player_name()
	speed = speed or vector.length(player:get_velocity())

	local old = oldparticles[pname]
	if old then
		minetest.delete_particlespawner(old, pname)
		oldparticles[pname] = nil
	end

	if speed < 2 then return end

	local height = player:get_properties().eye_height
	oldparticles[pname] = minetest.add_particlespawner({
			amount = 20,
			time = 2,
			size = 1/8,
			expirationtime = 1,
			texture = "autotrek_particle.png",
			playername = pname,
			attached = player,
			glow = 14,
			minpos = vector.new(-1, height - 1, 0),
			maxpos = vector.new(1, height + 1, speed),
			acceleration = vector.new(0, 0, -speed),
		})
end

------------------------------------------------------------------------
-- TURN HANDLING

local function issolid(pos)
	local node = minetest.get_node_or_nil(pos)
	local def = node and minetest.registered_nodes[node.name]
	return def and def.walkable
end

local function iswall(pos)
	return issolid(pos) and issolid(vector.offset(pos, 0, 1, 0))
end

local function isfloor(pos)
	return not iswall(pos) and issolid(vector.offset(pos, 0, -1, 0))
end

local function turncheck90(player, pos, fwd, theta)
	local turn = vector.round(minetest.yaw_to_dir(theta + minetest.dir_to_yaw(fwd)))
	local function rel(f, t)
		return vector.add(vector.add(pos,
				vector.multiply(fwd, f)),
			vector.multiply(turn, t))
	end
	-- Check for a turn structure when player is on a 90 degree cardinal
	if isfloor(rel(0, 1)) and iswall(rel(0, -1)) and iswall(rel(-1, 1)) then
		-- A structure with a "short" wall will turn 45 degrees,
		-- a "long" wall will turn a full 90 degrees.
		if not iswall(rel(1, 1)) then theta = theta / 2 end
		player:set_look_horizontal(math_floor(player:get_look_horizontal()
				/ hpi + 0.5) * hpi + theta)
		return true
	end
end

local function checkallturns90(player, rpos, fwd)
	return turncheck90(player, rpos, fwd, hpi)
	or turncheck90(player, rpos, fwd, -hpi)
end

local function turncheck45(player, pos, fwd, theta)
	-- In this case, unlike in the 90 degree case, the fwd vector is not
	-- the direction we're traveling, but the direction we WOULD be traveling
	-- if the turn is accepted.
	local side = vector.round(minetest.yaw_to_dir(hpi + minetest.dir_to_yaw(fwd)))
	local function rel(f, s)
		return vector.add(vector.add(pos,
				vector.multiply(fwd, f)),
			vector.multiply(side, s))
	end
	if isfloor(rel(1, 0)) and iswall(rel(0, -1)) and iswall(rel(0, 1))
	and iswall(rel(1, -1)) and iswall(rel(1, 1)) then
		player:set_look_horizontal(math_floor(player:get_look_horizontal()
				/ qpi + 0.5) * qpi + theta)
		return true
	end
end

local function checkallturns45(player, rpos, besti)
	-- Readjust the facing angle by 45 degrees for each
	-- check, and accept any that has a guiding path
	-- with walls on either sides.
	local rt = alldirs[(besti - 2) % #alldirs + 1]
	local lt = alldirs[besti % #alldirs + 1]
	return turncheck45(player, rpos, lt, qpi)
	or turncheck45(player, rpos, rt, -qpi)
end

------------------------------------------------------------------------
-- PATH CORRECTION

local function isswimmable(pos)
	local node = minetest.get_node(pos)
	local def = minetest.registered_nodes[node.name] or {}
	return def.climbable or def.liquid_move_physics or def.liquidtype ~= "none"
end

local vzero = vector.new(0, 0, 0)
local function pathcorrect(player, ppos, bestv, swimcheck)
	-- If the player's yaw is not exact enough, then
	-- snap it to the exact direction. We have to
	-- allow for some floating point rounding error here
	-- though, as MT uses a float32 internally I think.
	local horz = player:get_look_horizontal()
	local nh = math_floor(horz / qpi + 0.5) * qpi
	if math_abs(horz - nh) > lookdirerr then
		player:set_look_horizontal(nh)
	end

	-- Determine if the player needs to or climb up: either
	-- they are sinking too far into a sinkable/swimmable
	-- node, or they're in one and the node in front of them
	-- is one they'd have to climb out for.
	local addvel = vzero
	if swimcheck and isswimmable(ppos)
	and (isswimmable(vector.offset(ppos, 0, 1, 0))
		or issolid(vector.add(ppos, bestv))) then
		addvel = vector.new(0, 10, 0)
	end

	-- Compute how far away from the grid centerline
	-- the player's path is and nudge them back into
	-- alignment with the exact centers of nodes.
	local across = vector.cross(bestv, vector.new(0, 1, 0))
	local spacing = (across.x ~= 0 and across.z ~= 0) and root2 or 1
	local offs = vector.dot(vector.new(ppos.x, 0, ppos.z), across)
	offs = offs / spacing
	offs = offs - math_floor(offs)
	if offs > griderr and offs < 0.5 then
		addvel = vector.add(addvel, vector.multiply(across, -gridforce))
	elseif offs >= 0.5 and offs < 1 - griderr then
		addvel = vector.add(addvel, vector.multiply(across, gridforce))
	end

	if addvel ~= vzero and not vector.equals(addvel, vzero) then
		player:add_velocity(addvel)
	end
end

------------------------------------------------------------------------
-- AUTOTREK STATE MACHINE

local disallowkeys = {
	left = true,
	right = true,
	down = true,
	dig = true,
	place = true,
	LMB = true,
	RMB = true,
	sneak = true,
}

local pdata = {}
local function checkplayer(player, dtime)
	local pname = player:get_player_name()
	local data = pdata[pname]
	if not data then
		data = {}
		pdata[pname] = data
	end

	-- Player must be walking forward continously
	-- and doing nothing else for at least a few
	-- seconds, otherwise deactivate
	local ctl = player:get_player_control()
	local fwd = ctl.up
	for k in pairs(disallowkeys) do
		if ctl[k] then
			fwd = false
			break
		end
	end
	if fwd then
		data.runtime = (data.runtime or 0) + dtime
	else
		while next(data) do
			data[next(data)] = nil
		end
		particles(player, pname, 0)
		return hudset(player)
	end
	if data.runtime < walktime then
		particles(player, pname, 0)
		return hudset(player)
	end

	-- Figure out which of the [semi-]cardinal directions
	-- is closest to the one the player is looking at, and
	-- how closely the player is pointed to that exact angle.
	local look = player:get_look_dir()
	local besti, bestv, bestd
	for i, v in pairs(alldirs) do
		local d = vector.dot(v, look)
		if (not bestv) or (d > bestd) then
			bestd = d
			bestv = v
			besti = i
		end
	end

	-- Display the appropriate HUD.
	local ppos = player:get_pos()
	local vpos = vector.offset(ppos, 0, player:get_properties().eye_height, 0)
	hudset(player, vector.add(vpos, vector.multiply(bestv, huddist)),
		bestd < lockmin and 24 or 192)

	-- The player must lock on close enough to the
	-- exact direction for a little while to activate
	-- the remaining features.
	if bestd >= lockmin then
		data.locktime = (data.locktime or 0) + dtime
	else
		data.locktime = 0
		data.fxtime = 0
	end
	if data.locktime < locktime then return end

	-- Draw particles.
	data.fxtime = (data.fxtime or 0) - dtime
	if data.fxtime <= 0 then
		particles(player, pname)
		data.fxtime = 1
	end

	local rpos = vector.round(ppos)

	-- Check for any turns.
	if bestv.z == 0 or bestv.x == 0 then
		if isfloor(rpos) and iswall(vector.add(rpos, bestv))
		and checkallturns90(player, rpos, bestv)
		then return end
	elseif isfloor(rpos) and checkallturns45(player, rpos, besti) then
		return
	end

	-- Apply path correction if no turns.
	local swimcheck
	data.swimtime = (data.swimtime or 0) - dtime
	if data.swimtime <= 0 then
		swimcheck = true
		data.swimtime = 1
	end
	return pathcorrect(player, ppos, bestv, swimcheck)
end

------------------------------------------------------------------------
-- MAIN LOOP

minetest.register_globalstep(function(dtime)
		for _, v in pairs(minetest.get_connected_players()) do
			checkplayer(v, dtime)
		end
	end)
