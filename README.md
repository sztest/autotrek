**AutoTrek** adds a long-distance travel system to most games (especially sandbox games) that is highly balanced compared to teleportation or other overpowered transportation mods.  You simply *walk* (or run, if available) the entire distance for your journey, and AutoTrek helps keep you on a precise, repeatable path.

You can use Minetest's **auto-forward** feature to automatically walk along paths designed for AutoTrek, including making some automatic turns.  You can design paths (often quite inexpensively) to reliably align and guide players on long distance trips.

### Activation

AutoTrek supports the **4 cardinal directions** (North, South, East, West) and the **4 diagonals** between them.

Pick one of the supported directions and **walk continuously forward** for a few seconds.  You will see a HUD appear with the AutoTrek icon.  If you **point directly at this HUD** for a second or so, you will "lock in" your travel direction, and AutoTrek will make fine adjustments to your direction and position to keep you centered on your path (terrain allowing).

While locked in to a travel direction, players will automatically **swim/climb upward** as necessary to stay afloat and clear simple obstacles such as shores.

Beware that when traveling diagonally, the player may experience "friction" against the edges of nodes that can push them off course, and the course correction may not always be enough to put the player back on the original path; they can end up on a path alongside the original.  You should carefully plan and modify diagonal paths for best reliability.

### Automatic Turns

When a player is "locked in" to a travel direction, certain structures, when detected, will cause the player to turn automatically and lock in another path 90 or 45 degrees from the previous one.

- When walking in one of the 4 cardinal directions, a wall in front of the player, with a wall to the left and a "post" to turn past on the right will cause the player to turn right.  Similarly, a wall on the right and a post on the left will turn left.
- If the wall is at least 2m wide, the player will turn 90 degrees.  Otherwise, if just a single wall is in front of the player, they will turn 45 degrees.
- When walking along a 45 degree angle, if the player is diverted into a tunnel that is at least 2m long with walls on either side, the player will turn and walk straight along the tunnel.
- Turns must generally be on level ground (no steps/jumps up or drops down) to work correctly,and there needs to be enough head clearance (2m) for the player.

*See the ContentDB screenshots for examples of the turn-guiding structures.*

- Additional material can be added to these structures (e.g. ceilings, some additional walls, other decorative structures).
- AutoTrek paths can be relatively easily built as underground tunnels.
- Lighting/visibility along the path is NOT necessary (these features work by "touch"), though dark tunnels may still be uncomfortable for players to navigate.
